#include <iostream>
#include <memory>
#include <algorithm>
#include <iterator>
#include <vector>
#include <array>
using namespace std;
/*Задана матрица размером NxM. Получить массив B, 
присвоив его k-му элементу значение 1, если элементы 
k-й строки матрицы упорядочены по убыванию, значение -1, если упорядочены
по возрастанию, и значение 0 − в противном случае. */


class Matrix
{
private:
    int height;
    int width;
    int** matrix;
public:
    Matrix()
    {
        height = 3;
        width = 3;
    }
    Matrix(short int h, short int w)
    {
        if (h > 0)
        {
            height = h;
        }
        else
        {
            height = 3;
        }
        if (w > 0)
        {
            width = w;
        }
        else
        {
            width = 3;
        }
        matrix = new int* [height];
        for (auto it = matrix; it < matrix + height; it++)
        {
            *it = new int[width];
        }
    }
    void fill_matrix()
    {
        int size = width * height - 5;
        for (auto it = matrix; it < matrix + height; it++)
        {
            for (auto it2 = *it; it2 < *it + width; it2++)
            {
                *it2 = rand() % 100;
            }
        }
    }

    void print_matrix()
    {
        cout << "A matrix: " << endl;
        for (auto it = matrix; it < matrix + height; it++)
        {
            for (auto it2 = *it; it2 < *it + width; it2++)
            {
                cout << *it2 << " ";
            }
            cout << endl;
        }
    }
    void clean_matrix()
    {
        for (auto it = matrix; it < matrix + height; it++)
        {
            delete[] *it;
        }
        delete[] matrix;
    }
    int get_value(int a, int b)
    {
        return(matrix[a][b]);
    }
    void fill_b()
    {
        unique_ptr<int[]> b(new int[height]);
        unique_ptr <int> iter; 
        for (int i = 0; i < height; i++)
        {
            b[i] = 1;
        }
        for (int i = 0; i < height; i++)
        {
            for (int j = 1; j < width; j++) //не нашла аналог is_sorted для убывания
            {
                if (matrix[i][j] > matrix[i][j - 1])
                {
                    b[i] = 0;
                }
            }
            if (b[i] != 1)
            {
                if (is_sorted(matrix[i], matrix[i] + width)) 
                {
                    b[i] = -1;
                }
            } 
        }
        cout << "B matrix: " << endl;
        for (int i = 0; i < height; i++)
        {
            cout << b[i] << " ";
        }
    }
};


int main() //не проходит при пустой инициализации
{
    short int m,n;
    cout << "Enter m" << endl;
    cin >> m;
    while (cin.fail())
    {
        cin.clear();
        cin.ignore();
        cout << "Inter m again" << endl;
        cin >> m;
    }
    cout << "Enter n" << endl;
    cin >> n;
    while (cin.fail())
    {
        cin.clear();
        cin.ignore();
        cout << "Inter n again" << endl;
        cin >> n;
    }
    Matrix a {m,n};
    a.fill_matrix(); //заполнение матрицы рандомными числами
    a.print_matrix(); //печать этой матрицы
    a.fill_b(); //вывод массива b на основе матрицы a. здесь использован unique_ptr
    a.clean_matrix(); //очищение матрицы
}

